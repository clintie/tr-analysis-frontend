## Triad Management Analysis V2

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>

Open [http://[REDACTED]:8002](http://[REDACTED]:3000) to view the graphs on the local network.

This app was written by Clint Watt, Oct 2019.

This is version 1.07; Dec 2019.

## Discussion

This React-based app uses data scraped from the Elexon database to monitor current
electricity and forecast daily Energy consumption in the UK, in order to predict the
highest consuption half-hours of the year.  These top 3 periods are named 'Triads' 
and for the basis for billing industial users a 'network usage charge'.  

Further info can be found [here](https://www.nationalgrideso.com/news/triads-why-three-magic-number).