import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  switch: {
    padding: '15px 0px 0px 15px',
  },
  cookies: {
    padding: '20px 0px  0px 15px',
    bottom: 0
  },
  formControlLabel: {
    fontSize: '1em'
  }
});

export { useStyles }