import {UPDATE_TRIAD_DATES, 
  UPDATE_TRIAD_DATE,
  SWITCH_ALL_DATA, 
  SHOW_OPTIONS_STATE, 
  ADD_NEW_DATUM, 
  UPDATE_NEW_DATA, 
  SET_FOCUS, 
  SWITCH_LEGEND,
  USE_TRIAD_DATES,
  UPDATE_PEAK_WATCH
} from './types'

const updateTriadDates = (triadDates) => {
  return {type: UPDATE_TRIAD_DATES, triadDates};
}

const updateTriadDate = (index, triadDate) => {
  return {type: UPDATE_TRIAD_DATE, index, triadDate};
}

const updateUsedTriadDates = (idx, value) => {
  return {type: USE_TRIAD_DATES, value, idx}
}

const switchAllData = () => ({type: SWITCH_ALL_DATA});

const clearNewData = () => {
  return {type: UPDATE_NEW_DATA};
}

const addNewData = (newDatum) => {
  return {type: ADD_NEW_DATUM, action: newDatum};
}

const updateState = (isUpdating) => {
  return {type: ADD_NEW_DATUM, isUpdating};
}

const updateFocus = () => {
  return {type: SET_FOCUS}
}

const switchLegend = () => ({type:SWITCH_LEGEND})

const setOptionsState = (showing) => ({type: SHOW_OPTIONS_STATE, showOptions: showing})

const updatePeakWatch = (idx, level, on, time) => ({type: UPDATE_PEAK_WATCH, idx, level, on, time})

export {
  updateTriadDates, 
  switchAllData, 
  setOptionsState, 
  clearNewData, 
  addNewData, 
  updateState, 
  updateFocus,
  switchLegend,
  updateUsedTriadDates,
  updateTriadDate,
  updatePeakWatch
}