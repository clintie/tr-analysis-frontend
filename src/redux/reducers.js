import {
  UPDATE_TRIAD_DATES, 
  SWITCH_ALL_DATA, 
  SHOW_OPTIONS_STATE, 
  ADD_NEW_DATUM, 
  UPDATE_NEW_DATA, 
  UPDATE_STATE, 
  SET_FOCUS, 
  SWITCH_LEGEND,
  USE_TRIAD_DATES,
  UPDATE_TRIAD_DATE,
  UPDATE_PEAK_WATCH
} from './types'

import {today} from '../util/date'

const initialState = {
  triadDates: [today()],
  useTriadDates:[true, true, true, true, true, true],
  allDataSeries: false,
  completeLegend: false,
  showOptions: false,
  newGraphData: [],
  updating: false,
  focus: true,
  peakwatch:[
    {on: false, level:45000, time: '16:30:00'},
    {on: false, level:46000, time: '17:00:00'},
    {on: false, level:47000, time: '17:30:00'}
  ]
}

export const triadDateReducer = (state = initialState, action) => {
  // console.log(action)
  switch (action.type) {
    case UPDATE_TRIAD_DATES:
      return {...state, triadDates: action.triadDates}
    case UPDATE_STATE:
      return {...state, updating: action}     
    case SET_FOCUS:
      return {...state, focus: !state.focus}       
    case ADD_NEW_DATUM:
      return {...state, newGraphData: [...state.newGraphData, action.action]}
    case UPDATE_NEW_DATA:
      return {...state, newGraphData: []}
    case SWITCH_LEGEND: 
      return {...state, completeLegend: !state.completeLegend}      
    case SWITCH_ALL_DATA: 
      return {...state, allDataSeries: !state.allDataSeries}
    case SHOW_OPTIONS_STATE: 
      return {...state, showOptions: action.showOptions}
    case USE_TRIAD_DATES:
      let useTriadDates = state.useTriadDates.slice();
      useTriadDates[action.idx] = action.value;
      return {...state, useTriadDates}
    case UPDATE_TRIAD_DATE:
      let triadDates = state.triadDates.slice();
      triadDates[action.index] = action.triadDate;
      return {...state, triadDates}
    case UPDATE_PEAK_WATCH:
      let peakwatch = state.peakwatch.slice();
      if (action.level) peakwatch[action.idx].level = parseInt(action.level);
      if (action.on !==null) peakwatch[action.idx].on = action.on;
      if (action.time) peakwatch[action.idx].time = action.time;
      return {...state, peakwatch};
    default:
      return {...state}
  }

}
