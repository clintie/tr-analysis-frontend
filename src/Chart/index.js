import React from 'react';
import {connect} from 'react-redux';

//amCharts
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// import am4themes_material from "@amcharts/amcharts4/themes/material";

// material UI
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'

import {chartConfig} from './config'
import {createSeries, sourceFields} from './series'
import {switchAllData, clearNewData, updateUsedTriadDates} from '../redux/actions'
import dayjs from 'dayjs'
// import {tsDataMaxMin} from '../util/array';
// import tests from './tests'

am4core.useTheme(am4themes_animated);
am4core.options.minPolylineStep = 5;

class Chart extends React.Component {

  constructor() {
    super();
    this.allSeries = [];
  }
  
  componentDidMount = () => this.init()

  init = () => {
    const {data} = this.props;
    if (this.chart) {
      this.chart.dispose();
      this.chart=null;
    }
    this.chart = am4core.create("chartdiv", am4charts.XYChart);
    chartConfig(this.chart, this.props.switchAllData, this.props.showCompleteLegend);
    

    // this.chart.events.on("beforedatavalidated", (ev) => {

    //   this.chart.data.sort((a, b) => {
    //     if (a.ts > b.ts) return 1;
    //     if (a.ts < b.ts) return -1;
    //     return 0;
    //   });
    //   //this.chart.data.invalidateData();
    // });
    
    this.createAllLineSeries(this.props.dates);

    var groupBy = function(xs, key) {
      return xs.reduce(function(rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, {});
    };

    //console.log(groupBy(data));
    // let d = groupBy(data, 'ts');
    // console.log(d, data)
    // let d1 = (d[Object.keys(d)[0]])

    this.chart.data = data;
  }

  createAllLineSeries(allDates = []) {
    allDates.forEach((day, idx) => {
      let dayIsToday = day === dayjs().format('MM/DD/YYYY')
      sourceFields.forEach(field => {
        let series = createSeries(this.chart, day, field.name, field.friendly, dayIsToday, this.props.allSeries, idx)
        if (series) {
          this.allSeries.push(series);
          console.log('setting updateUsedTriadDates');

          this.props.updateUsedTriadDates(idx, true);  // by default make 
        }
      })
    })
  }

  action = (key) => <>
    <Button onClick={() => { this.props.closeSnackbar(key) }}>
        {'Dismiss'}
    </Button>
  </> 

  componentWillUnmount = () =>  this.chart && this.chart.dispose();

  componentDidUpdate=(prev) => {
    // legend content changed from min/full?  
    if (prev.showCompleteLegend !== this.props.showCompleteLegend) {
      this.allSeries.forEach(series=> {
        series.hiddenInLegend = !series.inAbbreviatedSet && !this.props.showCompleteLegend})
    }


    if (this.props.graphData.length) {
      this.props.graphData.forEach(item => {
          this.chart.addData(item);
          console.log(item)
      });
      this.chart.invalidateRawData();
      console.log(this.chart.data)
      this.props.clearNewData();
    }
  }

  render = () =><Grid container justify='center'>
    <Grid item xs={12} md={11}>
      <div id="chartdiv" style={{ width: "100%", height: "90vh" }}></div>
    </Grid>
  </Grid>
}

const mapStateToProps = (state) => ({
  showCompleteLegend: state.completeLegend,
  allSeries: state.allDataSeries,
  dates: state.triadDates,
  graphData: state.newGraphData,
  updated: state.updating
})

const mapDispatchToProps = { switchAllData, clearNewData, updateUsedTriadDates }

export default connect(mapStateToProps, mapDispatchToProps)(Chart);
