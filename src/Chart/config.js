import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

const chartConfig = (chart, legendExtendHandler) => {
  chart.strokeWidth = 2;
  chart.cursor = new am4charts.XYCursor();
  chart.scrollbarX = new am4core.Scrollbar();
  chart.scrollbarY = new am4core.Scrollbar();
  let legend = new am4charts.Legend();
  chart.legend = legend;

  let mW = new am4charts.ValueAxis();
  mW.extraMax = 0.05;  // cope with max being exceeded (ie. a new triad record!)
  mW.numberFormatter.numberFormat='#a'

  let TSlabel = new am4core.Label('UTC');
  TSlabel.text = 'local time';
  let TS= new am4charts.DateAxis();
  TS.title = TSlabel;


  let axis = chart.xAxes.push(TS);
  axis.renderer.minGridDistance = 60;
  axis.renderer.maxGridDistance = 60;
  chart.yAxes.push(mW); 
  chart.legend.reverseOrder = true;

  // essentially a temp bigfix:  allow zoom in when selecting a data range, BUT prevent default behaviour of 
  // zoom-out to existing range max/min on receiving new data.  suspect this is a limitation of graph package
  mW.keepSelection=true;
  chart.events.on('down', (ev) => mW.keepSelection=false);
  chart.events.on('up', (ev) => setTimeout(()=>mW.keepSelection=true, 1000));
}

export {chartConfig}