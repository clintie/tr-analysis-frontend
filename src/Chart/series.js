import maxContrastColors from '../util/colors';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import dayjs from 'dayjs';

const sourceFields = [
    { name:'demand_outturn', friendly: 'TSD', notify:true},
    { name:'demand_forecast', friendly: 'Forecast TSD', notify:true},
    { name:'rolling_tsd', friendly: `D'md Outturn`, notify:false},  
    { name:'rolling_triad_demand', friendly: 'Rolling Triad', notify:true},         // Previously Inst Triad D
    { name:'calculated_triad_demand', friendly: 'Rolling TSD', notify:true},        // Previously Triad D
    //{ name:'ngrt_tsd', friendly: 'TD NG (Net TSD NGIC)”', notify:true},        // TODO
    { name:'NGRT', friendly: 'NGRT', notify: true}
];

const createSeries = (chart, day, fieldName, friendlyName, inAbbreviatedSet=true, showAllSets=false, seriesIndex=2) => {
  let isToday = day===dayjs().format('MM/DD/YYYY');
  let series;
  
  //TODO: hack to reduce data points
  if (!isToday && fieldName !=='rolling_triad_demand' && !showAllSets) return null;

  switch (fieldName) {
    case 'rolling_tsd':
        return null;  // don't add as explicit series, just used for calculated_triad_demand calculation.
        
    case 'demand_outturn':
        series=chart.series.push(new am4charts.StepLineSeries());
        series.stroke=am4core.color(maxContrastColors[seriesIndex+3]);
        series.tooltip.pointerOrientation = 'up'
        series.strokeWidth=3;
        series.strokeDasharray='8'
        series.inAbbreviatedSet = inAbbreviatedSet;
        break;

    case 'demand_forecast':
        series=chart.series.push(new am4charts.StepLineSeries());
        series.stroke=am4core.color(maxContrastColors[seriesIndex+3]);
        series.tooltip.pointerOrientation = 'down'
        series.strokeWidth=3;
        series.strokeDasharray='1 1 2'
        series.inAbbreviatedSet = inAbbreviatedSet;
        break;

    case 'calculated_triad_demand':
        series=chart.series.push(new am4charts.LineSeries());
        series.tooltip.pointerOrientation = 'up'
        series.stroke=am4core.color(maxContrastColors[seriesIndex+3]);
        series.strokeWidth=1
        series.inAbbreviatedSet = inAbbreviatedSet;
        break;

    case 'rolling_triad_demand':
        series=chart.series.push(new am4charts.LineSeries());
        series.tooltip.pointerOrientation = 'right'
        series.stroke=am4core.color(maxContrastColors[seriesIndex+3]);
        series.strokeWidth=3;
        series.inAbbreviatedSet = inAbbreviatedSet;
        break;

    case 'NGRT':
        series=chart.series.push(new am4charts.LineSeries());
        series.tooltip.pointerOrientation = 'left'
        series.stroke=am4core.color(maxContrastColors[seriesIndex+3]);
        series.strokeWidth=3;
        series.inAbbreviatedSet = inAbbreviatedSet;
        series.strokeDasharray='3 3 3';
        break;

    default:
        console.log('ERR: '+ fieldName);
        return null
    }
    
    let bullet = series.bullets.push(new am4core.Circle());
    bullet.radius = 1.1;
    //series.minBulletDistance = 2.5;
    bullet.tooltipText=`{name}  {${fieldName+postfix(day)}} `
    series.tooltip.getFillFromObject = false;
    series.tooltip.background.fill = am4core.color(maxContrastColors[seriesIndex+3]);
    
    // series.hidden = !isToday && !fieldName.includes('rolling_triad_demand')
    series.name = (friendlyName || fieldName) + ' ' + dayjs(day).format('DD/MM') 
    series.dataFields.dateX = "ts";
    series.dataFields.valueY = fieldName+postfix(day)

    series.hiddenInLegend = !inAbbreviatedSet && !showAllSets;  //always show abbreviated set, hide the rest when not showing all sets
    return series;
}

const postfix = (day) => '_'+ dayjs(day).format('DDMMYYYY')

const findDataSeries = (allSeries, seriesY) => allSeries.find(series => series.dataFields.valueY === seriesY);

export {createSeries, findDataSeries, sourceFields}