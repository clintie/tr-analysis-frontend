export const SD = [
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 1,
      "demand_forecast": 22056,
      "demand_outturn": 21815,
      "report_ts": "2019-08-06T22:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 2,
      "demand_forecast": 21329,
      "demand_outturn": 21195,
      "report_ts": "2019-08-06T23:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 3,
      "demand_forecast": 21300,
      "demand_outturn": 21202,
      "report_ts": "2019-08-06T23:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 4,
      "demand_forecast": 21159,
      "demand_outturn": 21237,
      "report_ts": "2019-08-07T00:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 5,
      "demand_forecast": 21208,
      "demand_outturn": 21086,
      "report_ts": "2019-08-07T00:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 6,
      "demand_forecast": 21073,
      "demand_outturn": 20858,
      "report_ts": "2019-08-07T01:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 7,
      "demand_forecast": 20812,
      "demand_outturn": 20712,
      "report_ts": "2019-08-07T01:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 8,
      "demand_forecast": 20834,
      "demand_outturn": 20741,
      "report_ts": "2019-08-07T02:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 9,
      "demand_forecast": 21028,
      "demand_outturn": 21026,
      "report_ts": "2019-08-07T02:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 10,
      "demand_forecast": 21174,
      "demand_outturn": 20835,
      "report_ts": "2019-08-07T03:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 11,
      "demand_forecast": 20559,
      "demand_outturn": 20552,
      "report_ts": "2019-08-07T03:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 12,
      "demand_forecast": 21139,
      "demand_outturn": 20999,
      "report_ts": "2019-08-07T04:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 13,
      "demand_forecast": 23189,
      "demand_outturn": 23094,
      "report_ts": "2019-08-07T04:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 14,
      "demand_forecast": 25030,
      "demand_outturn": 24681,
      "report_ts": "2019-08-07T05:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 15,
      "demand_forecast": 27089,
      "demand_outturn": 26729,
      "report_ts": "2019-08-07T05:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 16,
      "demand_forecast": 28404,
      "demand_outturn": 28093,
      "report_ts": "2019-08-07T06:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 17,
      "demand_forecast": 29644,
      "demand_outturn": 29324,
      "report_ts": "2019-08-07T06:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 18,
      "demand_forecast": 30080,
      "demand_outturn": 29527,
      "report_ts": "2019-08-07T07:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 19,
      "demand_forecast": 30180,
      "demand_outturn": 29702,
      "report_ts": "2019-08-07T07:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 20,
      "demand_forecast": 29976,
      "demand_outturn": 29313,
      "report_ts": "2019-08-07T08:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 21,
      "demand_forecast": 29763,
      "demand_outturn": 28861,
      "report_ts": "2019-08-07T08:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 22,
      "demand_forecast": 29580,
      "demand_outturn": 28624,
      "report_ts": "2019-08-07T09:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 23,
      "demand_forecast": 29529,
      "demand_outturn": 28783,
      "report_ts": "2019-08-07T09:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 24,
      "demand_forecast": 29442,
      "demand_outturn": 28669,
      "report_ts": "2019-08-07T10:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 25,
      "demand_forecast": 28326,
      "demand_outturn": 28682,
      "report_ts": "2019-08-07T10:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 26,
      "demand_forecast": 28324,
      "demand_outturn": 28389,
      "report_ts": "2019-08-07T11:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 27,
      "demand_forecast": 28414,
      "demand_outturn": 28225,
      "report_ts": "2019-08-07T11:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 28,
      "demand_forecast": 28436,
      "demand_outturn": 27844,
      "report_ts": "2019-08-07T12:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 29,
      "demand_forecast": 28180,
      "demand_outturn": 27760,
      "report_ts": "2019-08-07T12:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 30,
      "demand_forecast": 28220,
      "demand_outturn": 27503,
      "report_ts": "2019-08-07T13:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 31,
      "demand_forecast": 28303,
      "demand_outturn": 27683,
      "report_ts": "2019-08-07T13:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 32,
      "demand_forecast": 28568,
      "demand_outturn": 27875,
      "report_ts": "2019-08-07T14:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 33,
      "demand_forecast": 28923,
      "demand_outturn": 28277,
      "report_ts": "2019-08-07T14:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 34,
      "demand_forecast": 29682,
      "demand_outturn": 28997,
      "report_ts": "2019-08-07T15:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 35,
      "demand_forecast": 30439,
      "demand_outturn": 29950,
      "report_ts": "2019-08-07T15:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 36,
      "demand_forecast": 30896,
      "demand_outturn": 30462,
      "report_ts": "2019-08-07T16:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 37,
      "demand_forecast": 31211,
      "demand_outturn": 30817,
      "report_ts": "2019-08-07T16:35:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 38,
      "demand_forecast": 31580,
      "demand_outturn": 31211,
      "report_ts": "2019-08-07T17:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 39,
      "demand_forecast": 31218,
      "demand_outturn": 31304,
      "report_ts": "2019-08-07T17:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 40,
      "demand_forecast": 30980,
      "demand_outturn": 31242,
      "report_ts": "2019-08-07T18:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 41,
      "demand_forecast": 31096,
      "demand_outturn": 30860,
      "report_ts": "2019-08-07T18:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 42,
      "demand_forecast": 30980,
      "demand_outturn": 30454,
      "report_ts": "2019-08-07T19:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 43,
      "demand_forecast": 30668,
      "demand_outturn": 30706,
      "report_ts": "2019-08-07T19:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 44,
      "demand_forecast": 29972,
      "demand_outturn": 29853,
      "report_ts": "2019-08-07T20:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 45,
      "demand_forecast": 28387,
      "demand_outturn": 28398,
      "report_ts": "2019-08-07T20:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 46,
      "demand_forecast": 26589,
      "demand_outturn": 26832,
      "report_ts": "2019-08-07T21:00:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 47,
      "demand_forecast": 24789,
      "demand_outturn": 25305,
      "report_ts": "2019-08-07T21:30:00.000Z"
  },
  {
      "settlement_day": "2019-08-06T23:00:00.000Z",
      "settlement_period": 48,
      "demand_forecast": 23138,
      "demand_outturn": 23620,
      "report_ts": "2019-08-07T22:00:00.000Z"
  }
]

