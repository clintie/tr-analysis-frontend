export const SDR = [
  {
      "ts": "2019-08-06T23:00:00.000Z",
      "rolling_tsd": "21275",
      "rolling_triad_demand": "20587"
  },
  {
      "ts": "2019-08-06T23:05:00.000Z",
      "rolling_tsd": "21308",
      "rolling_triad_demand": "20510"
  },
  {
      "ts": "2019-08-06T23:10:00.000Z",
      "rolling_tsd": "21354",
      "rolling_triad_demand": "20376"
  },
  {
      "ts": "2019-08-06T23:15:00.000Z",
      "rolling_tsd": "21331",
      "rolling_triad_demand": "20353"
  },
  {
      "ts": "2019-08-06T23:20:00.000Z",
      "rolling_tsd": "21325",
      "rolling_triad_demand": "20282"
  },
  {
      "ts": "2019-08-06T23:25:00.000Z",
      "rolling_tsd": "21221",
      "rolling_triad_demand": "19954"
  },
  {
      "ts": "2019-08-06T23:30:00.000Z",
      "rolling_tsd": "21192",
      "rolling_triad_demand": "19924"
  },
  {
      "ts": "2019-08-06T23:35:00.000Z",
      "rolling_tsd": "21300",
      "rolling_triad_demand": "20022"
  },
  {
      "ts": "2019-08-06T23:40:00.000Z",
      "rolling_tsd": "21314",
      "rolling_triad_demand": "20037"
  },
  {
      "ts": "2019-08-06T23:45:00.000Z",
      "rolling_tsd": "21339",
      "rolling_triad_demand": "20062"
  },
  {
      "ts": "2019-08-06T23:50:00.000Z",
      "rolling_tsd": "21285",
      "rolling_triad_demand": "20006"
  },
  {
      "ts": "2019-08-06T23:55:00.000Z",
      "rolling_tsd": "21242",
      "rolling_triad_demand": "19976"
  },
  {
      "ts": "2019-08-07T00:00:00.000Z",
      "rolling_tsd": "21114",
      "rolling_triad_demand": "19700"
  },
  {
      "ts": "2019-08-07T00:05:00.000Z",
      "rolling_tsd": "21178",
      "rolling_triad_demand": "19672"
  },
  {
      "ts": "2019-08-07T00:10:00.000Z",
      "rolling_tsd": "21161",
      "rolling_triad_demand": "19658"
  },
  {
      "ts": "2019-08-07T00:15:00.000Z",
      "rolling_tsd": "21163",
      "rolling_triad_demand": "19654"
  },
  {
      "ts": "2019-08-07T00:20:00.000Z",
      "rolling_tsd": "21094",
      "rolling_triad_demand": "19582"
  },
  {
      "ts": "2019-08-07T00:25:00.000Z",
      "rolling_tsd": "21066",
      "rolling_triad_demand": "19352"
  },
  {
      "ts": "2019-08-07T00:30:00.000Z",
      "rolling_tsd": "21108",
      "rolling_triad_demand": "19276"
  },
  {
      "ts": "2019-08-07T00:35:00.000Z",
      "rolling_tsd": "21068",
      "rolling_triad_demand": "19279"
  },
  {
      "ts": "2019-08-07T00:40:00.000Z",
      "rolling_tsd": "20880",
      "rolling_triad_demand": "19280"
  },
  {
      "ts": "2019-08-07T00:45:00.000Z",
      "rolling_tsd": "20778",
      "rolling_triad_demand": "19179"
  },
  {
      "ts": "2019-08-07T00:50:00.000Z",
      "rolling_tsd": "20763",
      "rolling_triad_demand": "19161"
  },
  {
      "ts": "2019-08-07T00:55:00.000Z",
      "rolling_tsd": "20784",
      "rolling_triad_demand": "19184"
  },
  {
      "ts": "2019-08-07T01:00:00.000Z",
      "rolling_tsd": "20775",
      "rolling_triad_demand": "19194"
  },
  {
      "ts": "2019-08-07T01:05:00.000Z",
      "rolling_tsd": "20823",
      "rolling_triad_demand": "19105"
  },
  {
      "ts": "2019-08-07T01:10:00.000Z",
      "rolling_tsd": "20794",
      "rolling_triad_demand": "19077"
  },
  {
      "ts": "2019-08-07T01:15:00.000Z",
      "rolling_tsd": "20791",
      "rolling_triad_demand": "19064"
  },
  {
      "ts": "2019-08-07T01:20:00.000Z",
      "rolling_tsd": "20740",
      "rolling_triad_demand": "19023"
  },
  {
      "ts": "2019-08-07T01:25:00.000Z",
      "rolling_tsd": "20749",
      "rolling_triad_demand": "19040"
  },
  {
      "ts": "2019-08-07T01:30:00.000Z",
      "rolling_tsd": "20788",
      "rolling_triad_demand": "19090"
  },
  {
      "ts": "2019-08-07T01:35:00.000Z",
      "rolling_tsd": "20777",
      "rolling_triad_demand": "19078"
  },
  {
      "ts": "2019-08-07T01:40:00.000Z",
      "rolling_tsd": "20791",
      "rolling_triad_demand": "19097"
  },
  {
      "ts": "2019-08-07T01:45:00.000Z",
      "rolling_tsd": "20772",
      "rolling_triad_demand": "19077"
  },
  {
      "ts": "2019-08-07T01:50:00.000Z",
      "rolling_tsd": "20776",
      "rolling_triad_demand": "19079"
  },
  {
      "ts": "2019-08-07T01:55:00.000Z",
      "rolling_tsd": "20831",
      "rolling_triad_demand": "19130"
  },
  {
      "ts": "2019-08-07T02:00:00.000Z",
      "rolling_tsd": "21111",
      "rolling_triad_demand": "19216"
  },
  {
      "ts": "2019-08-07T02:05:00.000Z",
      "rolling_tsd": "21153",
      "rolling_triad_demand": "19243"
  },
  {
      "ts": "2019-08-07T02:10:00.000Z",
      "rolling_tsd": "21134",
      "rolling_triad_demand": "19228"
  },
  {
      "ts": "2019-08-07T02:15:00.000Z",
      "rolling_tsd": "21188",
      "rolling_triad_demand": "19290"
  },
  {
      "ts": "2019-08-07T02:20:00.000Z",
      "rolling_tsd": "21121",
      "rolling_triad_demand": "19431"
  },
  {
      "ts": "2019-08-07T02:25:00.000Z",
      "rolling_tsd": "20976",
      "rolling_triad_demand": "19196"
  },
  {
      "ts": "2019-08-07T02:30:00.000Z",
      "rolling_tsd": "20933",
      "rolling_triad_demand": "19286"
  },
  {
      "ts": "2019-08-07T02:35:00.000Z",
      "rolling_tsd": "21056",
      "rolling_triad_demand": "19338"
  },
  {
      "ts": "2019-08-07T02:40:00.000Z",
      "rolling_tsd": "21024",
      "rolling_triad_demand": "19331"
  },
  {
      "ts": "2019-08-07T02:45:00.000Z",
      "rolling_tsd": "20835",
      "rolling_triad_demand": "19395"
  },
  {
      "ts": "2019-08-07T02:50:00.000Z",
      "rolling_tsd": "20822",
      "rolling_triad_demand": "19402"
  },
  {
      "ts": "2019-08-07T02:55:00.000Z",
      "rolling_tsd": "20831",
      "rolling_triad_demand": "19524"
  },
  {
      "ts": "2019-08-07T03:00:00.000Z",
      "rolling_tsd": "20781",
      "rolling_triad_demand": "19640"
  },
  {
      "ts": "2019-08-07T03:05:00.000Z",
      "rolling_tsd": "20696",
      "rolling_triad_demand": "19883"
  },
  {
      "ts": "2019-08-07T03:10:00.000Z",
      "rolling_tsd": "20638",
      "rolling_triad_demand": "20035"
  },
  {
      "ts": "2019-08-07T03:15:00.000Z",
      "rolling_tsd": "20600",
      "rolling_triad_demand": "20026"
  },
  {
      "ts": "2019-08-07T03:20:00.000Z",
      "rolling_tsd": "20585",
      "rolling_triad_demand": "20004"
  },
  {
      "ts": "2019-08-07T03:25:00.000Z",
      "rolling_tsd": "20673",
      "rolling_triad_demand": "19852"
  },
  {
      "ts": "2019-08-07T03:30:00.000Z",
      "rolling_tsd": "20713",
      "rolling_triad_demand": "20105"
  },
  {
      "ts": "2019-08-07T03:35:00.000Z",
      "rolling_tsd": "20832",
      "rolling_triad_demand": "20255"
  },
  {
      "ts": "2019-08-07T03:40:00.000Z",
      "rolling_tsd": "20925",
      "rolling_triad_demand": "20349"
  },
  {
      "ts": "2019-08-07T03:45:00.000Z",
      "rolling_tsd": "21077",
      "rolling_triad_demand": "20504"
  },
  {
      "ts": "2019-08-07T03:50:00.000Z",
      "rolling_tsd": "21325",
      "rolling_triad_demand": "20750"
  },
  {
      "ts": "2019-08-07T03:55:00.000Z",
      "rolling_tsd": "21649",
      "rolling_triad_demand": "21068"
  },
  {
      "ts": "2019-08-07T04:00:00.000Z",
      "rolling_tsd": "22123",
      "rolling_triad_demand": "21412"
  },
  {
      "ts": "2019-08-07T04:05:00.000Z",
      "rolling_tsd": "22698",
      "rolling_triad_demand": "22098"
  },
  {
      "ts": "2019-08-07T04:10:00.000Z",
      "rolling_tsd": "23062",
      "rolling_triad_demand": "22415"
  },
  {
      "ts": "2019-08-07T04:15:00.000Z",
      "rolling_tsd": "23347",
      "rolling_triad_demand": "22707"
  },
  {
      "ts": "2019-08-07T04:20:00.000Z",
      "rolling_tsd": "23623",
      "rolling_triad_demand": "22984"
  },
  {
      "ts": "2019-08-07T04:25:00.000Z",
      "rolling_tsd": "23854",
      "rolling_triad_demand": "23224"
  },
  {
      "ts": "2019-08-07T04:30:00.000Z",
      "rolling_tsd": "24074",
      "rolling_triad_demand": "23534"
  },
  {
      "ts": "2019-08-07T04:35:00.000Z",
      "rolling_tsd": "24391",
      "rolling_triad_demand": "23816"
  },
  {
      "ts": "2019-08-07T04:40:00.000Z",
      "rolling_tsd": "24595",
      "rolling_triad_demand": "24055"
  },
  {
      "ts": "2019-08-07T04:45:00.000Z",
      "rolling_tsd": "24767",
      "rolling_triad_demand": "24248"
  },
  {
      "ts": "2019-08-07T04:50:00.000Z",
      "rolling_tsd": "25016",
      "rolling_triad_demand": "24572"
  },
  {
      "ts": "2019-08-07T04:55:00.000Z",
      "rolling_tsd": "25393",
      "rolling_triad_demand": "24942"
  },
  {
      "ts": "2019-08-07T05:00:00.000Z",
      "rolling_tsd": "25781",
      "rolling_triad_demand": "25459"
  },
  {
      "ts": "2019-08-07T05:05:00.000Z",
      "rolling_tsd": "26446",
      "rolling_triad_demand": "26085"
  },
  {
      "ts": "2019-08-07T05:10:00.000Z",
      "rolling_tsd": "26689",
      "rolling_triad_demand": "26301"
  },
  {
      "ts": "2019-08-07T05:15:00.000Z",
      "rolling_tsd": "26987",
      "rolling_triad_demand": "26572"
  },
  {
      "ts": "2019-08-07T05:20:00.000Z",
      "rolling_tsd": "27226",
      "rolling_triad_demand": "26775"
  },
  {
      "ts": "2019-08-07T05:25:00.000Z",
      "rolling_tsd": "27385",
      "rolling_triad_demand": "26922"
  },
  {
      "ts": "2019-08-07T05:30:00.000Z",
      "rolling_tsd": "27583",
      "rolling_triad_demand": "27138"
  },
  {
      "ts": "2019-08-07T05:35:00.000Z",
      "rolling_tsd": "27858",
      "rolling_triad_demand": "27372"
  },
  {
      "ts": "2019-08-07T05:40:00.000Z",
      "rolling_tsd": "28023",
      "rolling_triad_demand": "27486"
  },
  {
      "ts": "2019-08-07T05:45:00.000Z",
      "rolling_tsd": "28201",
      "rolling_triad_demand": "27627"
  },
  {
      "ts": "2019-08-07T05:50:00.000Z",
      "rolling_tsd": "28417",
      "rolling_triad_demand": "27795"
  },
  {
      "ts": "2019-08-07T05:55:00.000Z",
      "rolling_tsd": "28697",
      "rolling_triad_demand": "28023"
  },
  {
      "ts": "2019-08-07T06:00:00.000Z",
      "rolling_tsd": "29067",
      "rolling_triad_demand": "28344"
  },
  {
      "ts": "2019-08-07T06:05:00.000Z",
      "rolling_tsd": "29291",
      "rolling_triad_demand": "28507"
  },
  {
      "ts": "2019-08-07T06:10:00.000Z",
      "rolling_tsd": "29388",
      "rolling_triad_demand": "28554"
  },
  {
      "ts": "2019-08-07T06:15:00.000Z",
      "rolling_tsd": "29495",
      "rolling_triad_demand": "28611"
  },
  {
      "ts": "2019-08-07T06:20:00.000Z",
      "rolling_tsd": "29528",
      "rolling_triad_demand": "28593"
  },
  {
      "ts": "2019-08-07T06:25:00.000Z",
      "rolling_tsd": "29528",
      "rolling_triad_demand": "28542"
  },
  {
      "ts": "2019-08-07T06:30:00.000Z",
      "rolling_tsd": "29552",
      "rolling_triad_demand": "28551"
  },
  {
      "ts": "2019-08-07T06:35:00.000Z",
      "rolling_tsd": "29521",
      "rolling_triad_demand": "28520"
  },
  {
      "ts": "2019-08-07T06:40:00.000Z",
      "rolling_tsd": "29456",
      "rolling_triad_demand": "28455"
  },
  {
      "ts": "2019-08-07T06:45:00.000Z",
      "rolling_tsd": "29471",
      "rolling_triad_demand": "28470"
  },
  {
      "ts": "2019-08-07T06:50:00.000Z",
      "rolling_tsd": "29589",
      "rolling_triad_demand": "28599"
  },
  {
      "ts": "2019-08-07T06:55:00.000Z",
      "rolling_tsd": "29684",
      "rolling_triad_demand": "28694"
  },
  {
      "ts": "2019-08-07T07:00:00.000Z",
      "rolling_tsd": "29878",
      "rolling_triad_demand": "28888"
  },
  {
      "ts": "2019-08-07T07:05:00.000Z",
      "rolling_tsd": "29749",
      "rolling_triad_demand": "28759"
  },
  {
      "ts": "2019-08-07T07:10:00.000Z",
      "rolling_tsd": "29666",
      "rolling_triad_demand": "28676"
  },
  {
      "ts": "2019-08-07T07:15:00.000Z",
      "rolling_tsd": "29684",
      "rolling_triad_demand": "28694"
  },
  {
      "ts": "2019-08-07T07:20:00.000Z",
      "rolling_tsd": "29724",
      "rolling_triad_demand": "28734"
  },
  {
      "ts": "2019-08-07T07:25:00.000Z",
      "rolling_tsd": "29616",
      "rolling_triad_demand": "28626"
  },
  {
      "ts": "2019-08-07T07:30:00.000Z",
      "rolling_tsd": "29498",
      "rolling_triad_demand": "28508"
  },
  {
      "ts": "2019-08-07T07:35:00.000Z",
      "rolling_tsd": "29375",
      "rolling_triad_demand": "28385"
  },
  {
      "ts": "2019-08-07T07:40:00.000Z",
      "rolling_tsd": "29258",
      "rolling_triad_demand": "28268"
  },
  {
      "ts": "2019-08-07T07:45:00.000Z",
      "rolling_tsd": "29190",
      "rolling_triad_demand": "28200"
  },
  {
      "ts": "2019-08-07T07:50:00.000Z",
      "rolling_tsd": "29318",
      "rolling_triad_demand": "28328"
  },
  {
      "ts": "2019-08-07T07:55:00.000Z",
      "rolling_tsd": "29346",
      "rolling_triad_demand": "28356"
  },
  {
      "ts": "2019-08-07T08:00:00.000Z",
      "rolling_tsd": "29060",
      "rolling_triad_demand": "28070"
  },
  {
      "ts": "2019-08-07T08:05:00.000Z",
      "rolling_tsd": "29008",
      "rolling_triad_demand": "28018"
  },
  {
      "ts": "2019-08-07T08:10:00.000Z",
      "rolling_tsd": "28916",
      "rolling_triad_demand": "27926"
  },
  {
      "ts": "2019-08-07T08:15:00.000Z",
      "rolling_tsd": "28867",
      "rolling_triad_demand": "27877"
  },
  {
      "ts": "2019-08-07T08:20:00.000Z",
      "rolling_tsd": "28837",
      "rolling_triad_demand": "27847"
  },
  {
      "ts": "2019-08-07T08:25:00.000Z",
      "rolling_tsd": "28794",
      "rolling_triad_demand": "27804"
  },
  {
      "ts": "2019-08-07T08:30:00.000Z",
      "rolling_tsd": "28742",
      "rolling_triad_demand": "27752"
  },
  {
      "ts": "2019-08-07T08:35:00.000Z",
      "rolling_tsd": "28571",
      "rolling_triad_demand": "27581"
  },
  {
      "ts": "2019-08-07T08:40:00.000Z",
      "rolling_tsd": "28521",
      "rolling_triad_demand": "27531"
  },
  {
      "ts": "2019-08-07T08:45:00.000Z",
      "rolling_tsd": "28568",
      "rolling_triad_demand": "27578"
  },
  {
      "ts": "2019-08-07T08:50:00.000Z",
      "rolling_tsd": "28688",
      "rolling_triad_demand": "27698"
  },
  {
      "ts": "2019-08-07T08:55:00.000Z",
      "rolling_tsd": "28761",
      "rolling_triad_demand": "27771"
  },
  {
      "ts": "2019-08-07T09:00:00.000Z",
      "rolling_tsd": "28839",
      "rolling_triad_demand": "27849"
  },
  {
      "ts": "2019-08-07T09:05:00.000Z",
      "rolling_tsd": "28794",
      "rolling_triad_demand": "27804"
  },
  {
      "ts": "2019-08-07T09:10:00.000Z",
      "rolling_tsd": "28772",
      "rolling_triad_demand": "27782"
  },
  {
      "ts": "2019-08-07T09:15:00.000Z",
      "rolling_tsd": "28833",
      "rolling_triad_demand": "27843"
  },
  {
      "ts": "2019-08-07T09:20:00.000Z",
      "rolling_tsd": "28821",
      "rolling_triad_demand": "27831"
  },
  {
      "ts": "2019-08-07T09:25:00.000Z",
      "rolling_tsd": "28698",
      "rolling_triad_demand": "27708"
  },
  {
      "ts": "2019-08-07T09:30:00.000Z",
      "rolling_tsd": "28680",
      "rolling_triad_demand": "27690"
  },
  {
      "ts": "2019-08-07T09:35:00.000Z",
      "rolling_tsd": "28347",
      "rolling_triad_demand": "27357"
  },
  {
      "ts": "2019-08-07T09:40:00.000Z",
      "rolling_tsd": "28531",
      "rolling_triad_demand": "27541"
  },
  {
      "ts": "2019-08-07T09:45:00.000Z",
      "rolling_tsd": "28729",
      "rolling_triad_demand": "27739"
  },
  {
      "ts": "2019-08-07T09:50:00.000Z",
      "rolling_tsd": "28801",
      "rolling_triad_demand": "27811"
  },
  {
      "ts": "2019-08-07T09:55:00.000Z",
      "rolling_tsd": "28878",
      "rolling_triad_demand": "27888"
  },
  {
      "ts": "2019-08-07T10:00:00.000Z",
      "rolling_tsd": "28966",
      "rolling_triad_demand": "27976"
  },
  {
      "ts": "2019-08-07T10:05:00.000Z",
      "rolling_tsd": "28880",
      "rolling_triad_demand": "27890"
  },
  {
      "ts": "2019-08-07T10:10:00.000Z",
      "rolling_tsd": "28419",
      "rolling_triad_demand": "27429"
  },
  {
      "ts": "2019-08-07T10:15:00.000Z",
      "rolling_tsd": "28643",
      "rolling_triad_demand": "27653"
  },
  {
      "ts": "2019-08-07T10:20:00.000Z",
      "rolling_tsd": "28631",
      "rolling_triad_demand": "27641"
  },
  {
      "ts": "2019-08-07T10:25:00.000Z",
      "rolling_tsd": "28563",
      "rolling_triad_demand": "27573"
  },
  {
      "ts": "2019-08-07T10:30:00.000Z",
      "rolling_tsd": "28448",
      "rolling_triad_demand": "27458"
  },
  {
      "ts": "2019-08-07T10:35:00.000Z",
      "rolling_tsd": "28425",
      "rolling_triad_demand": "27435"
  },
  {
      "ts": "2019-08-07T10:40:00.000Z",
      "rolling_tsd": "28361",
      "rolling_triad_demand": "27371"
  },
  {
      "ts": "2019-08-07T10:45:00.000Z",
      "rolling_tsd": "28331",
      "rolling_triad_demand": "27341"
  },
  {
      "ts": "2019-08-07T10:50:00.000Z",
      "rolling_tsd": "28377",
      "rolling_triad_demand": "27387"
  },
  {
      "ts": "2019-08-07T10:55:00.000Z",
      "rolling_tsd": "28416",
      "rolling_triad_demand": "27426"
  },
  {
      "ts": "2019-08-07T11:00:00.000Z",
      "rolling_tsd": "28435",
      "rolling_triad_demand": "27436"
  },
  {
      "ts": "2019-08-07T11:05:00.000Z",
      "rolling_tsd": "28343",
      "rolling_triad_demand": "27345"
  },
  {
      "ts": "2019-08-07T11:10:00.000Z",
      "rolling_tsd": "28415",
      "rolling_triad_demand": "27418"
  },
  {
      "ts": "2019-08-07T11:15:00.000Z",
      "rolling_tsd": "28260",
      "rolling_triad_demand": "27263"
  },
  {
      "ts": "2019-08-07T11:20:00.000Z",
      "rolling_tsd": "28090",
      "rolling_triad_demand": "27092"
  },
  {
      "ts": "2019-08-07T11:25:00.000Z",
      "rolling_tsd": "28005",
      "rolling_triad_demand": "27007"
  },
  {
      "ts": "2019-08-07T11:30:00.000Z",
      "rolling_tsd": "27967",
      "rolling_triad_demand": "26969"
  },
  {
      "ts": "2019-08-07T11:35:00.000Z",
      "rolling_tsd": "27841",
      "rolling_triad_demand": "26843"
  },
  {
      "ts": "2019-08-07T11:40:00.000Z",
      "rolling_tsd": "27827",
      "rolling_triad_demand": "26829"
  },
  {
      "ts": "2019-08-07T11:45:00.000Z",
      "rolling_tsd": "27794",
      "rolling_triad_demand": "26797"
  },
  {
      "ts": "2019-08-07T11:50:00.000Z",
      "rolling_tsd": "27731",
      "rolling_triad_demand": "26735"
  },
  {
      "ts": "2019-08-07T11:55:00.000Z",
      "rolling_tsd": "27979",
      "rolling_triad_demand": "26983"
  },
  {
      "ts": "2019-08-07T12:00:00.000Z",
      "rolling_tsd": "28098",
      "rolling_triad_demand": "27102"
  },
  {
      "ts": "2019-08-07T12:05:00.000Z",
      "rolling_tsd": "28197",
      "rolling_triad_demand": "27201"
  },
  {
      "ts": "2019-08-07T12:10:00.000Z",
      "rolling_tsd": "27798",
      "rolling_triad_demand": "26802"
  },
  {
      "ts": "2019-08-07T12:15:00.000Z",
      "rolling_tsd": "27576",
      "rolling_triad_demand": "26580"
  },
  {
      "ts": "2019-08-07T12:20:00.000Z",
      "rolling_tsd": "27523",
      "rolling_triad_demand": "26527"
  },
  {
      "ts": "2019-08-07T12:25:00.000Z",
      "rolling_tsd": "27411",
      "rolling_triad_demand": "26415"
  },
  {
      "ts": "2019-08-07T12:30:00.000Z",
      "rolling_tsd": "27414",
      "rolling_triad_demand": "26418"
  },
  {
      "ts": "2019-08-07T12:35:00.000Z",
      "rolling_tsd": "27417",
      "rolling_triad_demand": "26421"
  },
  {
      "ts": "2019-08-07T12:40:00.000Z",
      "rolling_tsd": "27462",
      "rolling_triad_demand": "26466"
  },
  {
      "ts": "2019-08-07T12:45:00.000Z",
      "rolling_tsd": "27557",
      "rolling_triad_demand": "26561"
  },
  {
      "ts": "2019-08-07T12:50:00.000Z",
      "rolling_tsd": "27578",
      "rolling_triad_demand": "26582"
  },
  {
      "ts": "2019-08-07T12:55:00.000Z",
      "rolling_tsd": "27645",
      "rolling_triad_demand": "26655"
  },
  {
      "ts": "2019-08-07T13:00:00.000Z",
      "rolling_tsd": "27707",
      "rolling_triad_demand": "26717"
  },
  {
      "ts": "2019-08-07T13:05:00.000Z",
      "rolling_tsd": "27548",
      "rolling_triad_demand": "26558"
  },
  {
      "ts": "2019-08-07T13:10:00.000Z",
      "rolling_tsd": "27595",
      "rolling_triad_demand": "26605"
  },
  {
      "ts": "2019-08-07T13:15:00.000Z",
      "rolling_tsd": "27737",
      "rolling_triad_demand": "26747"
  },
  {
      "ts": "2019-08-07T13:20:00.000Z",
      "rolling_tsd": "27780",
      "rolling_triad_demand": "26790"
  },
  {
      "ts": "2019-08-07T13:25:00.000Z",
      "rolling_tsd": "27806",
      "rolling_triad_demand": "26816"
  },
  {
      "ts": "2019-08-07T13:30:00.000Z",
      "rolling_tsd": "27898",
      "rolling_triad_demand": "26908"
  },
  {
      "ts": "2019-08-07T13:35:00.000Z",
      "rolling_tsd": "27811",
      "rolling_triad_demand": "26821"
  },
  {
      "ts": "2019-08-07T13:40:00.000Z",
      "rolling_tsd": "27826",
      "rolling_triad_demand": "26836"
  },
  {
      "ts": "2019-08-07T13:45:00.000Z",
      "rolling_tsd": "27939",
      "rolling_triad_demand": "26949"
  },
  {
      "ts": "2019-08-07T13:50:00.000Z",
      "rolling_tsd": "27967",
      "rolling_triad_demand": "26977"
  },
  {
      "ts": "2019-08-07T13:55:00.000Z",
      "rolling_tsd": "27935",
      "rolling_triad_demand": "26945"
  },
  {
      "ts": "2019-08-07T14:00:00.000Z",
      "rolling_tsd": "28047",
      "rolling_triad_demand": "27057"
  },
  {
      "ts": "2019-08-07T14:05:00.000Z",
      "rolling_tsd": "28167",
      "rolling_triad_demand": "27177"
  },
  {
      "ts": "2019-08-07T14:10:00.000Z",
      "rolling_tsd": "27606",
      "rolling_triad_demand": "26616"
  },
  {
      "ts": "2019-08-07T14:15:00.000Z",
      "rolling_tsd": "27850",
      "rolling_triad_demand": "26860"
  },
  {
      "ts": "2019-08-07T14:20:00.000Z",
      "rolling_tsd": "27202",
      "rolling_triad_demand": "26666"
  },
  {
      "ts": "2019-08-07T14:25:00.000Z",
      "rolling_tsd": "28616",
      "rolling_triad_demand": "27626"
  },
  {
      "ts": "2019-08-07T14:30:00.000Z",
      "rolling_tsd": "28053",
      "rolling_triad_demand": "27063"
  },
  {
      "ts": "2019-08-07T14:35:00.000Z",
      "rolling_tsd": "28846",
      "rolling_triad_demand": "27856"
  },
  {
      "ts": "2019-08-07T14:40:00.000Z",
      "rolling_tsd": "28948",
      "rolling_triad_demand": "27958"
  },
  {
      "ts": "2019-08-07T14:45:00.000Z",
      "rolling_tsd": "29086",
      "rolling_triad_demand": "28096"
  },
  {
      "ts": "2019-08-07T14:50:00.000Z",
      "rolling_tsd": "29206",
      "rolling_triad_demand": "28216"
  },
  {
      "ts": "2019-08-07T14:55:00.000Z",
      "rolling_tsd": "29219",
      "rolling_triad_demand": "28229"
  },
  {
      "ts": "2019-08-07T15:00:00.000Z",
      "rolling_tsd": "29577",
      "rolling_triad_demand": "28587"
  },
  {
      "ts": "2019-08-07T15:05:00.000Z",
      "rolling_tsd": "29808",
      "rolling_triad_demand": "28818"
  },
  {
      "ts": "2019-08-07T15:10:00.000Z",
      "rolling_tsd": "29863",
      "rolling_triad_demand": "28873"
  },
  {
      "ts": "2019-08-07T15:15:00.000Z",
      "rolling_tsd": "30027",
      "rolling_triad_demand": "29037"
  },
  {
      "ts": "2019-08-07T15:20:00.000Z",
      "rolling_tsd": "30150",
      "rolling_triad_demand": "29160"
  },
  {
      "ts": "2019-08-07T15:25:00.000Z",
      "rolling_tsd": "30406",
      "rolling_triad_demand": "29416"
  },
  {
      "ts": "2019-08-07T15:30:00.000Z",
      "rolling_tsd": "30456",
      "rolling_triad_demand": "29466"
  },
  {
      "ts": "2019-08-07T15:35:00.000Z",
      "rolling_tsd": "30435",
      "rolling_triad_demand": "29445"
  },
  {
      "ts": "2019-08-07T15:40:00.000Z",
      "rolling_tsd": "30433",
      "rolling_triad_demand": "29443"
  },
  {
      "ts": "2019-08-07T15:45:00.000Z",
      "rolling_tsd": "30434",
      "rolling_triad_demand": "29444"
  },
  {
      "ts": "2019-08-07T15:50:00.000Z",
      "rolling_tsd": "30501",
      "rolling_triad_demand": "29511"
  },
  {
      "ts": "2019-08-07T15:55:00.000Z",
      "rolling_tsd": "30575",
      "rolling_triad_demand": "29585"
  },
  {
      "ts": "2019-08-07T16:00:00.000Z",
      "rolling_tsd": "30718",
      "rolling_triad_demand": "29728"
  },
  {
      "ts": "2019-08-07T16:05:00.000Z",
      "rolling_tsd": "30779",
      "rolling_triad_demand": "29789"
  },
  {
      "ts": "2019-08-07T16:10:00.000Z",
      "rolling_tsd": "30789",
      "rolling_triad_demand": "29799"
  },
  {
      "ts": "2019-08-07T16:15:00.000Z",
      "rolling_tsd": "30847",
      "rolling_triad_demand": "29857"
  },
  {
      "ts": "2019-08-07T16:20:00.000Z",
      "rolling_tsd": "30865",
      "rolling_triad_demand": "29875"
  },
  {
      "ts": "2019-08-07T16:25:00.000Z",
      "rolling_tsd": "30882",
      "rolling_triad_demand": "29892"
  },
  {
      "ts": "2019-08-07T16:30:00.000Z",
      "rolling_tsd": "31074",
      "rolling_triad_demand": "30084"
  },
  {
      "ts": "2019-08-07T16:35:00.000Z",
      "rolling_tsd": "31209",
      "rolling_triad_demand": "30219"
  },
  {
      "ts": "2019-08-07T16:40:00.000Z",
      "rolling_tsd": "31284",
      "rolling_triad_demand": "30294"
  },
  {
      "ts": "2019-08-07T16:45:00.000Z",
      "rolling_tsd": "31283",
      "rolling_triad_demand": "30293"
  },
  {
      "ts": "2019-08-07T16:50:00.000Z",
      "rolling_tsd": "31300",
      "rolling_triad_demand": "30310"
  },
  {
      "ts": "2019-08-07T16:55:00.000Z",
      "rolling_tsd": "31212",
      "rolling_triad_demand": "30222"
  },
  {
      "ts": "2019-08-07T17:00:00.000Z",
      "rolling_tsd": "31277",
      "rolling_triad_demand": "30287"
  },
  {
      "ts": "2019-08-07T17:05:00.000Z",
      "rolling_tsd": "31311",
      "rolling_triad_demand": "30321"
  },
  {
      "ts": "2019-08-07T17:10:00.000Z",
      "rolling_tsd": "31299",
      "rolling_triad_demand": "30309"
  },
  {
      "ts": "2019-08-07T17:15:00.000Z",
      "rolling_tsd": "31311",
      "rolling_triad_demand": "30321"
  },
  {
      "ts": "2019-08-07T17:20:00.000Z",
      "rolling_tsd": "31329",
      "rolling_triad_demand": "30339"
  },
  {
      "ts": "2019-08-07T17:25:00.000Z",
      "rolling_tsd": "31339",
      "rolling_triad_demand": "30349"
  },
  {
      "ts": "2019-08-07T17:30:00.000Z",
      "rolling_tsd": "31428",
      "rolling_triad_demand": "30438"
  },
  {
      "ts": "2019-08-07T17:35:00.000Z",
      "rolling_tsd": "31282",
      "rolling_triad_demand": "30292"
  },
  {
      "ts": "2019-08-07T17:40:00.000Z",
      "rolling_tsd": "31248",
      "rolling_triad_demand": "30258"
  },
  {
      "ts": "2019-08-07T17:45:00.000Z",
      "rolling_tsd": "31262",
      "rolling_triad_demand": "30272"
  },
  {
      "ts": "2019-08-07T17:50:00.000Z",
      "rolling_tsd": "31192",
      "rolling_triad_demand": "30202"
  },
  {
      "ts": "2019-08-07T17:55:00.000Z",
      "rolling_tsd": "31075",
      "rolling_triad_demand": "30085"
  },
  {
      "ts": "2019-08-07T18:00:00.000Z",
      "rolling_tsd": "31166",
      "rolling_triad_demand": "30176"
  },
  {
      "ts": "2019-08-07T18:05:00.000Z",
      "rolling_tsd": "31159",
      "rolling_triad_demand": "30169"
  },
  {
      "ts": "2019-08-07T18:10:00.000Z",
      "rolling_tsd": "30971",
      "rolling_triad_demand": "29981"
  },
  {
      "ts": "2019-08-07T18:15:00.000Z",
      "rolling_tsd": "30801",
      "rolling_triad_demand": "29811"
  },
  {
      "ts": "2019-08-07T18:20:00.000Z",
      "rolling_tsd": "30599",
      "rolling_triad_demand": "29609"
  },
  {
      "ts": "2019-08-07T18:25:00.000Z",
      "rolling_tsd": "30509",
      "rolling_triad_demand": "29519"
  },
  {
      "ts": "2019-08-07T18:30:00.000Z",
      "rolling_tsd": "30362",
      "rolling_triad_demand": "29372"
  },
  {
      "ts": "2019-08-07T18:35:00.000Z",
      "rolling_tsd": "30318",
      "rolling_triad_demand": "29328"
  },
  {
      "ts": "2019-08-07T18:40:00.000Z",
      "rolling_tsd": "30421",
      "rolling_triad_demand": "29431"
  },
  {
      "ts": "2019-08-07T18:45:00.000Z",
      "rolling_tsd": "30478",
      "rolling_triad_demand": "29488"
  },
  {
      "ts": "2019-08-07T18:50:00.000Z",
      "rolling_tsd": "30524",
      "rolling_triad_demand": "29534"
  },
  {
      "ts": "2019-08-07T18:55:00.000Z",
      "rolling_tsd": "30619",
      "rolling_triad_demand": "29629"
  },
  {
      "ts": "2019-08-07T19:00:00.000Z",
      "rolling_tsd": "30774",
      "rolling_triad_demand": "29784"
  },
  {
      "ts": "2019-08-07T19:05:00.000Z",
      "rolling_tsd": "30717",
      "rolling_triad_demand": "29727"
  },
  {
      "ts": "2019-08-07T19:10:00.000Z",
      "rolling_tsd": "30782",
      "rolling_triad_demand": "29792"
  },
  {
      "ts": "2019-08-07T19:15:00.000Z",
      "rolling_tsd": "30804",
      "rolling_triad_demand": "29814"
  },
  {
      "ts": "2019-08-07T19:20:00.000Z",
      "rolling_tsd": "30717",
      "rolling_triad_demand": "29727"
  },
  {
      "ts": "2019-08-07T19:25:00.000Z",
      "rolling_tsd": "30495",
      "rolling_triad_demand": "29505"
  },
  {
      "ts": "2019-08-07T19:30:00.000Z",
      "rolling_tsd": "30338",
      "rolling_triad_demand": "29348"
  },
  {
      "ts": "2019-08-07T19:35:00.000Z",
      "rolling_tsd": "30114",
      "rolling_triad_demand": "29124"
  },
  {
      "ts": "2019-08-07T19:40:00.000Z",
      "rolling_tsd": "29869",
      "rolling_triad_demand": "28879"
  },
  {
      "ts": "2019-08-07T19:45:00.000Z",
      "rolling_tsd": "29814",
      "rolling_triad_demand": "28824"
  },
  {
      "ts": "2019-08-07T19:50:00.000Z",
      "rolling_tsd": "29636",
      "rolling_triad_demand": "28646"
  },
  {
      "ts": "2019-08-07T19:55:00.000Z",
      "rolling_tsd": "29373",
      "rolling_triad_demand": "28383"
  },
  {
      "ts": "2019-08-07T20:00:00.000Z",
      "rolling_tsd": "28971",
      "rolling_triad_demand": "27981"
  },
  {
      "ts": "2019-08-07T20:05:00.000Z",
      "rolling_tsd": "28837",
      "rolling_triad_demand": "27847"
  },
  {
      "ts": "2019-08-07T20:10:00.000Z",
      "rolling_tsd": "28608",
      "rolling_triad_demand": "27618"
  },
  {
      "ts": "2019-08-07T20:15:00.000Z",
      "rolling_tsd": "28280",
      "rolling_triad_demand": "27290"
  },
  {
      "ts": "2019-08-07T20:20:00.000Z",
      "rolling_tsd": "28032",
      "rolling_triad_demand": "27042"
  },
  {
      "ts": "2019-08-07T20:25:00.000Z",
      "rolling_tsd": "27738",
      "rolling_triad_demand": "26748"
  },
  {
      "ts": "2019-08-07T20:30:00.000Z",
      "rolling_tsd": "27430",
      "rolling_triad_demand": "26440"
  },
  {
      "ts": "2019-08-07T20:35:00.000Z",
      "rolling_tsd": "27192",
      "rolling_triad_demand": "26202"
  },
  {
      "ts": "2019-08-07T20:40:00.000Z",
      "rolling_tsd": "26978",
      "rolling_triad_demand": "25988"
  },
  {
      "ts": "2019-08-07T20:45:00.000Z",
      "rolling_tsd": "26754",
      "rolling_triad_demand": "25764"
  },
  {
      "ts": "2019-08-07T20:50:00.000Z",
      "rolling_tsd": "26465",
      "rolling_triad_demand": "25475"
  },
  {
      "ts": "2019-08-07T20:55:00.000Z",
      "rolling_tsd": "26251",
      "rolling_triad_demand": "25261"
  },
  {
      "ts": "2019-08-07T21:00:00.000Z",
      "rolling_tsd": "26061",
      "rolling_triad_demand": "25093"
  },
  {
      "ts": "2019-08-07T21:05:00.000Z",
      "rolling_tsd": "25838",
      "rolling_triad_demand": "24922"
  },
  {
      "ts": "2019-08-07T21:10:00.000Z",
      "rolling_tsd": "25372",
      "rolling_triad_demand": "24505"
  },
  {
      "ts": "2019-08-07T21:15:00.000Z",
      "rolling_tsd": "25071",
      "rolling_triad_demand": "24254"
  },
  {
      "ts": "2019-08-07T21:20:00.000Z",
      "rolling_tsd": "24854",
      "rolling_triad_demand": "24088"
  },
  {
      "ts": "2019-08-07T21:25:00.000Z",
      "rolling_tsd": "24659",
      "rolling_triad_demand": "23944"
  },
  {
      "ts": "2019-08-07T21:30:00.000Z",
      "rolling_tsd": "24364",
      "rolling_triad_demand": "23698"
  },
  {
      "ts": "2019-08-07T21:35:00.000Z",
      "rolling_tsd": "24068",
      "rolling_triad_demand": "23426"
  },
  {
      "ts": "2019-08-07T21:40:00.000Z",
      "rolling_tsd": "23767",
      "rolling_triad_demand": "23125"
  },
  {
      "ts": "2019-08-07T21:45:00.000Z",
      "rolling_tsd": "23479",
      "rolling_triad_demand": "22838"
  },
  {
      "ts": "2019-08-07T21:50:00.000Z",
      "rolling_tsd": "23296",
      "rolling_triad_demand": "22655"
  },
  {
      "ts": "2019-08-07T21:55:00.000Z",
      "rolling_tsd": "23032",
      "rolling_triad_demand": "22391"
  },
  {
      "ts": "2019-08-07T22:00:00.000Z",
      "rolling_tsd": "22880",
      "rolling_triad_demand": "22240"
  },
  {
      "ts": "2019-08-07T22:05:00.000Z",
      "rolling_tsd": "22790",
      "rolling_triad_demand": "22150"
  },
  {
      "ts": "2019-08-07T22:10:00.000Z",
      "rolling_tsd": "22506",
      "rolling_triad_demand": "21879"
  },
  {
      "ts": "2019-08-07T22:15:00.000Z",
      "rolling_tsd": "22396",
      "rolling_triad_demand": "21796"
  },
  {
      "ts": "2019-08-07T22:20:00.000Z",
      "rolling_tsd": "22278",
      "rolling_triad_demand": "21703"
  },
  {
      "ts": "2019-08-07T22:25:00.000Z",
      "rolling_tsd": "22148",
      "rolling_triad_demand": "21596"
  },
  {
      "ts": "2019-08-07T22:30:00.000Z",
      "rolling_tsd": "22097",
      "rolling_triad_demand": "21544"
  }
]
