export const IC = [
  {
      "fuel_type": "INTEW",
      "settlement_period": 1,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 1,
      "mw": 1362
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 1,
      "mw": -150
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 1,
      "mw": 222
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 1,
      "mw": 670
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 2,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 2,
      "mw": 1362
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 2,
      "mw": -212
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 2,
      "mw": 202
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 2,
      "mw": 670
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 3,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 3,
      "mw": 1362
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 3,
      "mw": -254
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 3,
      "mw": 194
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 3,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 4,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 4,
      "mw": 1364
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 4,
      "mw": -292
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 4,
      "mw": 188
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 4,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 5,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 5,
      "mw": 1118
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 5,
      "mw": -232
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 5,
      "mw": -280
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 5,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 6,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 6,
      "mw": 1118
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 6,
      "mw": -250
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 6,
      "mw": -298
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 6,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 7,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 7,
      "mw": 1118
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 7,
      "mw": -246
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 7,
      "mw": -174
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 7,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 8,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 8,
      "mw": 1114
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 8,
      "mw": -222
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 8,
      "mw": -172
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 8,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 9,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 9,
      "mw": 1362
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 9,
      "mw": -178
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 9,
      "mw": -422
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 9,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 10,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 10,
      "mw": 1364
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 10,
      "mw": -146
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 10,
      "mw": -426
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 10,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 11,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 11,
      "mw": 1364
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 11,
      "mw": -28
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 11,
      "mw": 0
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 11,
      "mw": 668
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 12,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 12,
      "mw": 1362
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 12,
      "mw": -18
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 12,
      "mw": 0
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 12,
      "mw": 660
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 13,
      "mw": 0
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 13,
      "mw": 1554
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 13,
      "mw": 14
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 13,
      "mw": -220
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 13,
      "mw": 250
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 14,
      "mw": -38
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 14,
      "mw": 1552
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 14,
      "mw": -78
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 14,
      "mw": -232
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 14,
      "mw": 242
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 15,
      "mw": -200
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 15,
      "mw": 1678
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 15,
      "mw": -196
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 15,
      "mw": 0
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 15,
      "mw": 4
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 16,
      "mw": -314
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 16,
      "mw": 1678
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 16,
      "mw": -236
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 16,
      "mw": 0
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 16,
      "mw": 34
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 17,
      "mw": -452
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 17,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 17,
      "mw": -372
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 17,
      "mw": 300
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 17,
      "mw": 868
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 18,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 18,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 18,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 18,
      "mw": 328
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 18,
      "mw": 910
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 19,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 19,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 19,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 19,
      "mw": 660
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 19,
      "mw": 948
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 20,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 20,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 20,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 20,
      "mw": 668
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 20,
      "mw": 948
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 21,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 21,
      "mw": 1512
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 21,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 21,
      "mw": 170
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 21,
      "mw": 942
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 22,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 22,
      "mw": 1500
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 22,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 22,
      "mw": 152
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 22,
      "mw": 942
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 23,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 23,
      "mw": 1358
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 23,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 23,
      "mw": 278
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 23,
      "mw": 920
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 24,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 24,
      "mw": 1358
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 24,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 24,
      "mw": 288
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 24,
      "mw": 920
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 25,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 25,
      "mw": 1458
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 25,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 25,
      "mw": 738
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 25,
      "mw": 866
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 26,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 26,
      "mw": 1458
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 26,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 26,
      "mw": 748
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 26,
      "mw": 866
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 27,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 27,
      "mw": 1896
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 27,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 27,
      "mw": 402
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 27,
      "mw": 838
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 28,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 28,
      "mw": 1896
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 28,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 28,
      "mw": 394
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 28,
      "mw": 840
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 29,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 29,
      "mw": 1896
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 29,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 29,
      "mw": 270
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 29,
      "mw": 838
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 30,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 30,
      "mw": 1894
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 30,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 30,
      "mw": 268
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 30,
      "mw": 838
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 31,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 31,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 31,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 31,
      "mw": 180
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 31,
      "mw": 838
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 32,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 32,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 32,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 32,
      "mw": 180
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 32,
      "mw": 838
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 33,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 33,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 33,
      "mw": -364
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 33,
      "mw": 622
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 33,
      "mw": 868
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 34,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 34,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 34,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 34,
      "mw": 638
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 34,
      "mw": 868
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 35,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 35,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 35,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 35,
      "mw": 942
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 35,
      "mw": 942
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 36,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 36,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 36,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 36,
      "mw": 948
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 36,
      "mw": 942
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 37,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 37,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 37,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 37,
      "mw": 1000
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 37,
      "mw": 1000
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 38,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 38,
      "mw": 1998
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 38,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 38,
      "mw": 1000
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 38,
      "mw": 1000
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 39,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 39,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 39,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 39,
      "mw": 1000
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 39,
      "mw": 1000
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 40,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 40,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 40,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 40,
      "mw": 1002
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 40,
      "mw": 1000
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 41,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 41,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 41,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 41,
      "mw": 1002
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 41,
      "mw": 1000
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 42,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 42,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 42,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 42,
      "mw": 1002
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 42,
      "mw": 1000
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 43,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 43,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 43,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 43,
      "mw": 1000
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 43,
      "mw": 968
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 44,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 44,
      "mw": 1996
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 44,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 44,
      "mw": 1000
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 44,
      "mw": 968
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 45,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 45,
      "mw": 1798
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 45,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 45,
      "mw": 904
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 45,
      "mw": 776
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 46,
      "mw": -536
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 46,
      "mw": 1804
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 46,
      "mw": -454
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 46,
      "mw": 902
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 46,
      "mw": 774
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 47,
      "mw": -474
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 47,
      "mw": 1504
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 47,
      "mw": -394
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 47,
      "mw": 760
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 47,
      "mw": 700
  },
  {
      "fuel_type": "INTEW",
      "settlement_period": 48,
      "mw": -310
  },
  {
      "fuel_type": "INTFR",
      "settlement_period": 48,
      "mw": 1506
  },
  {
      "fuel_type": "INTIRL",
      "settlement_period": 48,
      "mw": -336
  },
  {
      "fuel_type": "INTNED",
      "settlement_period": 48,
      "mw": 758
  },
  {
      "fuel_type": "INTNEM",
      "settlement_period": 48,
      "mw": 700
  }
]




