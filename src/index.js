import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom'

import './index.css';
import App from './App/App';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {triadDateReducer} from './redux/reducers'
import * as serviceWorker from './serviceWorker';
import {SnackbarProvider} from 'notistack';

const store = createStore(
  triadDateReducer, /* preloadedState, */
  //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <SnackbarProvider>
    <Provider store={store}>
      <Router>
        <Route path='/:d1?/:d2?/:d3?/:d4?/:d5?' component={App} />
      </Router>
    </Provider>
  </SnackbarProvider>
  , document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
