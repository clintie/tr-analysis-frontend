import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import {MuiPickersUtilsProvider} from '@material-ui/pickers'
import DatePicker from './TriadDatePicker';
import {connect} from 'react-redux'
import {updateTriadDates, setOptionsState} from '../../redux/actions'
import DateFnsUtils from '@date-io/date-fns';
import ShowLegend from './Legend';
import AllSeries from './AllSeries';
import Focus from './Focus';
import Grid from '@material-ui/core/Grid';
import Alarm from './Alarm'
import Typography from '@material-ui/core/Typography';
import dayjs from 'dayjs'
import Header from './Header'


const useStyles = makeStyles({
  list: {
    width: 350,
  },
  alarm: {
    marginTop: 50,
    borderTop: 'solid 1px #cccccc'
  },
  alarmContent: {
    marginLeft:10
  }
});
 
function TemporaryDrawer(props) {
  let dates = props.dates.slice();
  const classes = useStyles();
  const toggleDrawer = (side, open) => event => {

    if (open || event.target.className === 'MuiBackdrop-root' 
      || (event.type === 'keydown' && event.key === 'Escape')) 
       props.setOptionsState(open);

    return;
  }

  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <Grid container direction='column' justify='space-between' alignItems='center'>
        <Grid item xs={12}>
          <Typography variant="h4" className={classes.header}>
              Settings
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <List>
          <Header text={'Triad candidates'}/>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DatePicker value={dates[0]} today={true} idx={0} id='today' disabled={true} readOnly={true}/>
              <DatePicker value={dates[1]} checked={props.useTriadDates[1]} idx={1} clearable={!dates[2]}/>
              <DatePicker value={dates[2]} checked={props.useTriadDates[2]} idx={2} hidden={!dates[1]} clearable={!dates[3]}/>
              <DatePicker value={dates[3]} checked={props.useTriadDates[3]} idx={3} hidden={!dates[2]} clearable={!dates[4]}/>
              <DatePicker value={dates[4]} checked={props.useTriadDates[4]} idx={4} hidden={!dates[3]} clearable={!dates[5]}/>
              <DatePicker value={dates[5]} checked={props.useTriadDates[5]} idx={5} hidden={!dates[4]} />
            </MuiPickersUtilsProvider>
          </List>
          <Divider />
          <Header text={'Graph options'}/>
          <ShowLegend />
          <AllSeries />
          <Focus />

        </Grid>
      </Grid>
      {/* <Grid item xs={12} className={classes.alarm}>
        <Header text={'Alarm on Inst Triad D, if:'}/>
        <Alarm className={classes.alarmContent} value={props.peaks[0].level} time={dayjs().hour(16).minute(30)} on={props.peaks[0].on} idx={0}/>
        <Alarm className={classes.alarmContent} value={props.peaks[1].level} time={dayjs().hour(17).minute(0)}  on={props.peaks[1].on} idx={1}/>
        <Alarm className={classes.alarmContent} value={props.peaks[2].level} time={dayjs().hour(17).minute(30)} on={props.peaks[2].on} idx={2}/>
      </Grid> */}

    </div>
  );

  return <>
    <Drawer open={props.show} onClose={toggleDrawer('left', false)} id='drawer'>
      {sideList('left')}
    </Drawer>
  </>
}

const mapStateToProps = (state) => ({
  dates: state.triadDates,
  useTriadDates: state.useTriadDates,
  show: state.showOptions,
  peaks: state.peakwatch
})

const mapDispatchToProps = {
  updateTriadDates, setOptionsState
}

export default connect(mapStateToProps, mapDispatchToProps)(TemporaryDrawer);
