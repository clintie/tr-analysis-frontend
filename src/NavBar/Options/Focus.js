import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { useStyles } from '../../theme/styles';
import {connect} from 'react-redux';
import {updateFocus} from '../../redux/actions'
const FocusSwitch = (props) => {
  const classes = useStyles()

  return <FormGroup row className={classes.cookies}>
    <FormControlLabel
      control={
        <Switch 
          checked={props.focus}
          onChange={()=>props.updateFocus()}
          value='focus'
          color="secondary"
          size="small"
          inputProps={{ 'aria-label': 'primary checkbox' }} />
      }
      label="Focus 1430-2100 local time"
    />
  </FormGroup>
}

const mapStateToProps = state => ({
  focus: state.focus
})

const mapDispatchToProps = { updateFocus }

export default connect(mapStateToProps, mapDispatchToProps)(FocusSwitch);
