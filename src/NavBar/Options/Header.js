import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles'

export default (props) => {
    const useStyles = makeStyles(theme => ({
        header: {
            marginTop:20,
            marginBottom: 20,
            marginLeft: 5,
            color: '#333333'
          }
    }));

    const classes = useStyles();
    return <Typography variant="button" className={classes.header}>
        {props.text}
    </Typography>
}