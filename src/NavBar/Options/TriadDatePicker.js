import React  from 'react';
import {KeyboardDatePicker} from '@material-ui/pickers'
import {connect} from 'react-redux'
import dayjs from 'dayjs';
import ListItem from '@material-ui/core/ListItem';
import Checkbox from '@material-ui/core/Checkbox';

import {updateTriadDates, updateUsedTriadDates, updateTriadDate} from '../../redux/actions';

/**
 * Display MaterialUI's KeyboardDatePicker which allows (in this app) the selection of dates up to 'yesterday'.
 * The current date can also be shown if prop.today is set, but this will technically only
 * produce a read-only field without picker functionality.
 */
const DatePicker = (props) => {
  
  const handleUseDateChange = event => {
    console.log('handling change', props.idx, event.target.checked);
    props.updateUsedTriadDates(props.idx, event.target.checked);
  };

  const handleDateChange = event => {
    console.log('handling change', event);
    props.updateTriadDate(props.idx, event? dayjs(event).format('MM/DD/YYYY') : null)
    if (!event)  props.updateUsedTriadDates(props.idx, false);
  };

  return !props.hidden ? <>
   <div style={{ display: 'inline-flex' }}>
    <Checkbox
      checked={props.useDates[props.idx]}
      onChange={handleUseDateChange}
      disabled={props.readOnly || !props.value}
      inputProps={{'aria-label': 'primary checkbox' }}
    />
    <ListItem button>
      <KeyboardDatePicker
        margin="normal"
        id={"date-picker-dialog"+props.idx}
        label={!props.today ? `Triad candidate ${props.idx}` : 'Today'}
        format="dd/MM/yyyy"
        value={props.value || null} 
        maxDate={props.today? null : new Date(new Date().setDate(new Date().getDate()-1))}
        onChange={handleDateChange}
        clearable={props.clearable}
        disabled={props.disabled}
        KeyboardButtonProps={{'aria-label': 'change date'}}/>
      </ListItem>
 
    </div>
  </>: null}


  const mapStateToProps = (state) => ({
    dates: state.triadDates,
    useDates: state.useTriadDates
  })

  const mapDispatchToProps = {updateTriadDates, updateUsedTriadDates, updateTriadDate}
  
  export default connect(mapStateToProps, mapDispatchToProps)(DatePicker) 