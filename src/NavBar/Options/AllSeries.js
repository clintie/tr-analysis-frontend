import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { useStyles } from '../../theme/styles';
import {connect} from 'react-redux';
import {switchAllData} from '../../redux/actions';

const AllSeries = (props) => {
  const classes = useStyles()

  return <FormGroup row className={classes.cookies}>
    <FormControlLabel
      control={
        <Switch 
          checked={props.allDataSeries}
          onChange={props.switchAllData}
          value='historicForecasts'
          color="secondary"
          size="small"
          inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      }
      label="Retrieve historic forecasts"
    />
  </FormGroup>
}


const mapStateToProps = state => ({
  allDataSeries: state.allDataSeries
})

const mapDispatchToProps = { switchAllData }

export default connect(mapStateToProps, mapDispatchToProps)(AllSeries);
