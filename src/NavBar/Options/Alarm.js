import React from 'react';
import {KeyboardTimePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import Checkbox from '@material-ui/core/Checkbox'
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import {updatePeakWatch} from '../../redux/actions';
import {connect} from 'react-redux';

const Alarm = (props) => {
    const useStyles = makeStyles(theme => ({
        gridMargin: { marginTop: 10 },
        textField: { width: 75, marginRight:10 },
        dateField: { width: 100, marginLeft:10  }
    }))
    const classes = useStyles();

    const onChange = field => e => {console.log(e.target.value); props.updatePeakWatch(props.idx, e.target.value, null, null);}
    const onChangeCheckbox = e => props.updatePeakWatch(props.idx, null, e.target.checked, null);
    const onChangeTime = (date, stringValue) => props.updatePeakWatch(props.idx, null, null, stringValue);

    return <Grid item xs={12} className={classes.gridMargin}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Checkbox color="secondary" inputProps={{'aria-label': 'secondary checkbox'}} onChange={onChangeCheckbox} value={props.on}/>
            <TextField className={classes.textField} defaultValue={props.value} onChange={onChange('level')}/>
            <span>at</span>
            <KeyboardTimePicker className={classes.dateField} ampm={false} value={props.time} onChange={onChangeTime}/>
        </MuiPickersUtilsProvider>
    </Grid>
}

const mapDispatchToProps = { updatePeakWatch }

export default connect(null, mapDispatchToProps)(Alarm)