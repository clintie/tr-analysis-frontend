import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { useStyles } from '../../theme/styles';
import {switchLegend} from '../../redux/actions';
import {connect} from 'react-redux';


const AllowCookies = (props) => {
  const classes = useStyles();

  return <FormGroup row className={classes.cookies}>
    <FormControlLabel
      control={
        <Switch 
          checked={props.legend}
          onChange={props.switchLegend}
          value='allSeries'
          size='small'
          color="secondary"
          inputProps={{ 'aria-label': 'primary checkbox' }}
        />
      }
      label="Show complete legend"
    />
  </FormGroup>
}

const mapStateToProps = (state) => ({
  legend: state.completeLegend
})

const mapDispatchToProps = { switchLegend }; 

export default connect(mapStateToProps, mapDispatchToProps)(AllowCookies);
