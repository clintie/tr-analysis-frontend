import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {connect} from 'react-redux'
import {setOptionsState} from './../redux/actions'
import dayjs from 'dayjs';
import {BUILD} from '../util/config'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  dateList: {
    color: 'yellow'
  },
  buildInfo: {
    fontSize: '0.5em',
    marginLeft: 5,
    paddingTop: 3
  }
}));

function Navbar(props) {

  const classes = useStyles();
  let triadDates = props.triads.map(date=>dayjs(date).format('ddd D MMM')).join(', ')
  
  const clickHandler = () =>  {props.setOptionsState(!props.show)}
  return (
    <nav className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={clickHandler}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Triad Monitor
          </Typography>
          <Typography variant='subtitle2' className={classes.dateList}>
            {triadDates}
          </Typography>
          <Typography className={classes.buildInfo}>[BUILD {BUILD}]</Typography>
        </Toolbar>
      </AppBar>
    </nav>
  );
}

const mapStateToProps = (state) => ({
  show: state.showOptions,
  triads: state.triadDates
})

const mapDispatchToProps = {
  setOptionsState
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
