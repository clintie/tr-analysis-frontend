import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    color: '#777777',
    padding: '1.5em'
  },
});

export default (props) => {
  const classes = useStyles();
  return <Grid container justify="center"
    alignItems="center" 
    style={{textAlign: 'center', marginTop:'10vh'}}>
      <Grid item xs={12}>
        <Typography variant='h2' className={classes.root}>
          No triad data for the current day.
        </Typography>
        <Typography variant='h6' className={classes.root}>
          Filtering set to show triad data after 1430 local time.
        </Typography>
    </Grid>
  </Grid>
}