import React from 'react';
import Progress from '@material-ui/core/CircularProgress'
import Grid from '@material-ui/core/Grid';

export default (props) => <Grid container justify="center"
  alignItems="center" 
  style={{textAlign: 'center', marginTop:'10vh'}}>
    <Grid item xs={12}><Progress size={'40vh'} thickness={1}/>
  </Grid>
</Grid>
