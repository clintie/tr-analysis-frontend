import React from 'react';
import Navbar from '../NavBar'
import Chart from '../Chart'
import './App.css';
import {pollForData} from '../util/dbapi';
import Loading from './Loading';
import NoData from './NoData';
import {paramDatesToFull} from '../util/date';
import {connect} from 'react-redux';
import {updateTriadDates, addNewData, updateState} from '../redux/actions';
import { withSnackbar } from 'notistack';
import { sourceFields } from './../Chart/series'
import dayjs from 'dayjs';
import Options from '../NavBar/Options';
import {Prompt} from 'react-router'

class App extends React.Component {
  constructor() {
    super();
    this.state={initalDataLoaded: false, noData:false};
  }

  async pollTodaysData() {
    // todo: if data already exists in redux for day, do nothing unless that 'day' is today
    // note the chart's source data, 'this.data' is updated when we do a chart.addData() as part of the addNewData action
    let data = await pollForData([dayjs(new Date()).format('MM/DD/YYYY')], this.props.focus);
    
    data.forEach(newItem => {
      Object.keys(newItem).filter(key => key !=='ts').forEach(key=>{
        let exists = this.data.filter(item => item.ts===newItem.ts && item[key] === newItem[key]).length>0
        
        if (!exists) {
          this.props.addNewData({ts: newItem.ts, [key]:newItem[key]});
          // id the friendly field name from the key
          let field = sourceFields.find(source => source.name === key.slice(0,key.lastIndexOf('_')));

          if (field && field.notify) 
            this.props.enqueueSnackbar('Updated: ' + field.friendly, {variant: 'success', disableWindowBlurListener:true, autoHideDuration:2000});
        }
        exists=null;
      });   //TODO: check for every key in case dup time stamp received with diff data content
    })
    data=[];
  }

  init = () => {
    const {params} = this.props.match;
    //onbeforeunload = e => "Don't leave"
    this.setState({initalDataLoaded: false, noData:true});
    let dateSet = this.props.dates.length>1 ? this.props.dates : paramDatesToFull(params);
    let pollDateSet = dateSet.filter((d, i) => this.props.useDates[i])
    this.props.updateTriadDates(dateSet);

    // get the initial data load for the named days, then poll for data every x seconds;
    // additions to current day's data set should be pushed to graph via .addData
    pollForData(pollDateSet, this.props.focus).then((data)=> {
      this.data=data 
      this.setState({initalDataLoaded: true, noData: this.data.length===0})
      if (!this.polling) 
        this.polling = setInterval(() => this.pollTodaysData(), 10000);
    });
  }

  componentDidMount = () => this.init();



  componentDidUpdate = (prev) => {
    // if there's an update (focus 24h/6h has changed / show complete series), and we have data to show, reinit the graph totally
    if (this.state.initalDataLoaded && 
      (
        (prev.focus !== this.props.focus) ||
        (prev.showAllSeries !== this.props.showAllSeries) ||
        (prev.noData && !this.state.noData) ||
        (JSON.stringify(prev.dates) !== JSON.stringify(this.props.dates)) ||
        (JSON.stringify(prev.useDates) !== JSON.stringify(this.props.useDates))
      )
    ) {
      this.setState({initalDataLoaded: false}, ()=> {
        this.data=[];
        this.init();      
      });

    }
  }

  componentWillUnmount = () => {
    clearInterval(this.polling);
  }

  render = () => {
    let appRender;
    if (!this.state.initalDataLoaded) {
      appRender = <Loading/>
    } else if (this.state.initalDataLoaded && this.state.noData) {
      appRender = <NoData />
    } else if (this.state.initalDataLoaded && !this.state.noData) { 
      appRender = <Chart key='triadChart' data={this.data}/> 
    }
  
  return <>
    <Navbar />
    <Options />
    {appRender}
  </>
  }
}

const mapStateToProps = (state) => ({
  dates: state.triadDates,
  useDates: state.useTriadDates,
  showAllSeries: state.allDataSeries,
  focus: state.focus
})

const mapDispatchToProps = { updateTriadDates, addNewData, updateState }

export default connect(mapStateToProps, mapDispatchToProps)(withSnackbar(App));
