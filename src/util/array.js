// const arrayChanges = (oldSet, newSet, uniqueKey='ts') => {
//   let differences = [];
//   //newSet = newer (larger?) set
//   newSet.map(item => {
//     let match = oldSet.reverse().find(matchItem => matchItem[uniqueKey] === item[uniqueKey]);   // does object with matching unique value exist in both sets
//     if (!match)                                                                       // if no, record/push as new object
//       return differences.push({
//         // type: 'object', 
//       ...item});

//     // otherwise, loop thru individual key props for matched object
//     Object.keys(item).filter(key=>key !==uniqueKey).map(key => {            // ignoring the key field, loop through all new-object keys
//       if (!match.hasOwnProperty(key))                                       // does old object omit that property?
//         return differences.push({
//           [uniqueKey]:item[uniqueKey], 
//           // type:'property', 
//           [key]:item[key]
//         })  // then push as new property

//       if (match[key] !== item[key])                                                       // if property exists, but with different value
//         return differences.push({[uniqueKey]:item[uniqueKey], 
//           // type:'value', 
//           [key]:item[key]
//         }) 
        
//       return false;// push as value update
//     })
//     return true;
//   })

//   return differences;
// }

// const tsDataMaxMin = (data) => {
//   let min=10000;
//   let max;

//   data.filter(datum => datum.ts >= 30420000 && datum.ts <=40420000).forEach(fDatum=> {
//     Object.keys(fDatum).filter(key => (key !=='ts' && !key.includes('IC_'))).forEach(key=> {

//       if (!min || !max) min = max = fDatum[key];
//       if (fDatum[key] < min) min = fDatum[key];
//       if (fDatum[key] > max) max = fDatum[key];
//     })
//   })
//   return ({min, max})
// }

// const maxTSD = (data, dates) => {
//   console.log(dates, data)
// }

// export {arrayChanges, tsDataMaxMin, maxTSD};

