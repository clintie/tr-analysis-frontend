import {interconnectorData, 
  settlementDayForecast, 
  mergeDatasetByTs, 
  settlementDayRolling,
  ngrtRolling,
  demandForecast, 
  ngrtNet} from './settlements'
import axios from 'axios';
import dayjs from 'dayjs';
import {SERVER, 
  TEST_SERVER, 
  PORT} from './config';

const pollForData = async (days = [], focus=true) => {
  let data=[];
  console.log('polling days', days)
  // load up to 6 days (inc today) BOSS data
  let allPromises = [];
  
  days.forEach( (UTCday,i) => {
    let day = dayjs(UTCday).format('YYYY-MM-DD');
    allPromises.push(new Promise( async (res, rej)=> {
      let SDR = await axios.get(`http://${SERVER}:${PORT}/api/sdr/${day}/${focus.toString()}`, {timeout:3000}).then(r=>r.data);
      let IC = await axios.get(`http://${SERVER}:${PORT}/api/ic/${day}/${focus.toString()}`, {timeout:3000}).then(r=>r.data);
      let SD = await axios.get(`http://${SERVER}:${PORT}/api/sd/${day}/${focus.toString()}`, {timeout:3000}).then(r=>r.data);
      let NGRT = await axios.get(`http://${SERVER}:${PORT}/api/ngrt/${day}/${focus.toString()}`, {timeout:3000}).then(r=>r.data);
      let FCAST = await axios.get(`http://${SERVER}:${PORT}/api/fcast/${day}/${focus.toString()}`, {timeout:3000}).then(r=>r.data);
      let NGRT_TSD = await axios.get(`http://reports.vm.martinenergy.local/~iainh/get_data.py?table=ng_net_tsd&start=-24%20hours`, {timeout:3000}).then(r=>r.data);
      res({day, SDR, IC, SD, NGRT, FCAST, NGRT_TSD});
    }))
  });

  let allResults = await Promise.all(allPromises);

  allResults.forEach(dayResult => {     // for each day's results
    let {SD, IC, SDR, NGRT, FCAST, NGRT_TSD, day} = dayResult;
    let SDprocessed = settlementDayForecast(SD, day);
    let ICprocessed = interconnectorData(IC, (day));
    let SDRprocessed = settlementDayRolling(SDR, ICprocessed,  day);
    let NGRTprocessed = ngrtRolling(NGRT, day);
    let FCASTprocessed = demandForecast(FCAST, day);
    let NGRT_TSD_processed = ngrtNet(NGRT_TSD);

    console.log(FCASTprocessed)
    data = mergeDatasetByTs(day, data, 
      SDprocessed, 
      SDRprocessed,
      NGRTprocessed,
      FCASTprocessed,
      NGRT_TSD_processed
    );
  });

  return data;
}


export {pollForData}