import moment from 'moment';
import {datePostfix} from '../util/date';

const __START_OF_DAY = ' 00:00:00';
const today = new Date();
const DST = today.isDstObserved();

const getMsSinceMidnight = (d) => {
  var e = new Date(d);
  return d - e.setHours(0,0,0,0);  //TODO: account for BST more gracefully
}

/**
 * Return a settlement HH period (1 to 48) in a 05:00-base, POSIX time
 * @param {number} day MM/DD/YYYY
 * @param {number} sp   
 */
const settlementTsToHH = (day, sp) => {
  let spDay = new Date(day + __START_OF_DAY)
  var spHH = moment(new Date(spDay.getTime() + ((sp  ) * 1800000)).valueOf()).subtract(DST ? 90: 90,'m');

  return spHH;
}

/**
 * return a string array of unique fuel types from passed in Interconnector data
 * 
 * @param {Array} ICarray - the raw interconnector list for multiple HH's per in one day
 */
// const getUniqueIcFuelTypes = (ICarray) => {
//   let allFuelTypes = ICarray.map(icSpEntry => icSpEntry.fuel_type);
//   return Array.from(new Set(allFuelTypes));     // return only unique vals from array of all fuel types
// }


/**
 * Return an N sized array of summed IC import/exports per HH for a named day
 * @param {Array} ICarray the raw interconnector list for multiple HH's in one day
 * @param {Date} date day of IC data
 * @param {Boolean} zeroExportValues zero any mw values > 0
 */
const interconnectorData = (ICarray = [], date, zeroExportValues = true) => {
  
  // pluck out the timestamp and the mw value from raw API GET
  let result = ICarray.slice().map(item => ({
    ts: getMsSinceMidnight(settlementTsToHH(date, item.settlement_period)),
    mw: zeroExportValues ? (item.mw > 0) ? 0 : item.mw : item.mw,
  }));

  
  // sum up all mw values, grouping by ts

  let groupByTS = [];
  result.reduce((res, value) => {
    if (!res[value.ts]) {
      res[value.ts] = { ts: value.ts, mw: 0 };
      groupByTS.push(res[value.ts])
    }
    res[value.ts].mw += value.mw;
    return res;
  }, {});
  
  return groupByTS
}


const settlementDayForecast = (SDarray = [], date) => {
  // return only required data for merge with main dataset
  let data = SDarray.map((item)=> ({
    ts: getMsSinceMidnight(moment(item.report_ts).subtract(DST ? 90 : 90, 'm').valueOf()),  
    ['demand_outturn'+datePostfix(date)]: item.demand_outturn
  }));

  //TODO:  replace this workaround to hide 'wraparound midnight' from appearing on DST data
  return data.filter(item => item.ts<=81000000);
}

const demandForecast = (FCarray = [], date) => {
  // return only required data for merge with main dataset
  return FCarray.map((item)=> ({
    ts: getMsSinceMidnight(settlementTsToHH(date, item.settlement_period)),  // settlementTsToHH already includes DST shenanegans
    ['demand_forecast'+datePostfix(date)]: item.demand_forecast
  }));
}

const ngrtRolling = (NGRTarray = [], date) => {
  // return only required data for merge with main dataset
  return NGRTarray.map((item)=> ({
    ts: getMsSinceMidnight(moment(item.report_ts).subtract(DST ? 0 : 60, 'm')).valueOf(),
    ['NGRT'+datePostfix(date)]: item.mw
  }))
  .filter(item => item.ts<=81000000);  //TODO: remove wraparound midnight workaround.
}

const settlementDayRolling = (SDarray = [], IC=[], date) => {
  let rollingValues = SDarray.map((item)=> ({
    ts: getMsSinceMidnight(moment(item.ts).subtract(DST ? 120 : 60, 'm').valueOf()),
    ['rolling_tsd'+datePostfix(date)]: parseInt(item.rolling_tsd),
    ['rolling_triad_demand'+datePostfix(date)]: parseInt(item.rolling_triad_demand)
  }));


  // find IC half-hourly value ts matching settlement dataset ts, or first not exceeding the next IC ts;
  // and ADD this value to rolling_tsd for that 5m period to 
  // TODO: check whether it is actually the _prior_ HH IC value we use for calculated_triad_demand (in 
  //  which case, pull in IC in reverse ts order!!)
  const res = rollingValues.map(SDitem=>{
    let ICtsMatch = IC.find(ICitem => (ICitem.ts === SDitem.ts || ICitem.ts >= SDitem.ts ));
    let mw = ICtsMatch? ICtsMatch.mw : 0
    
    return {...SDitem, 
      ['IC'+datePostfix(date)]: mw, 
      ['calculated_triad_demand'+datePostfix(date)]: 
        parseInt(SDitem['rolling_tsd'+datePostfix(date)]) + mw
    }
  })
  .filter(item => item.ts<=81000000);  //TODO: remove wraparound midnight workaround.

  return res;
}

const ngrtNet = (dataset) => {
  let data = dataset.data;
  let todayString = moment().format('YYYY-MM-DD') + 'T00:00:00';
  let todayStart = Date.parse(todayString);
  data.forEach(d => {
    let _ts = Date.parse(d.x) - todayStart;
    d.ngrt_net = Math.trunc(d.y)

    delete d.x;
    delete d.y;
    d.ts = _ts;  // the number of ms into the day
  })
  data = data.filter(d => d.ts >=50400000)
  
  return data;
}

/**
 * Merge all datasets into an array of objects, ordered by timestamp
 * @param reducedData, the data from the last cycle of reductions
 * @param  {...any} datasets a rest param containing N ts-bearing object arrays  
 */
const mergeDatasetByTs = (day, reducedData, ...datasets) => {
  
  let result = reducedData.slice();

  // concept is to use the [0] dataset as to hold any data we've reduced already, the 'rest' of the datasets will be
  // looped thru in turn amassing a single set ordered by timestamp dataset, then 
  // add object properties to the extant array entries where .ts (timestamp) matches, OR push in NEW array entries
  // if dataset currently doesn't contain an entry for dataset's timestamp, remembering finally to
  // return the array sorted by ts for convenient consumption by graph component.
  datasets.forEach(donorSet => {
    donorSet.forEach(donorItem => {
      let mainSetItemLocation = (result.findIndex(item => item.ts === donorItem.ts));
      if (mainSetItemLocation > -1) {
        Object.assign(result[mainSetItemLocation], donorItem)
        result.push(donorItem)
      } else {
        result.push(donorItem)
      }
    })
  })
  let sortedResult = result.sort((a, b) => (a.ts > b.ts)? 1 : -1)
  return sortedResult;
}

export {
  interconnectorData, 
  settlementDayForecast, 
  mergeDatasetByTs,
  settlementDayRolling,
  ngrtRolling,
  demandForecast,
  ngrtNet
}