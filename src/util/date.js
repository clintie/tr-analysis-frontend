import dayjs from 'dayjs';

/**
 * convert a collection 4-char long URL params to dates.  if DDMM format is > today, interpret 
 * as last year, otherwise this year; return as array with [0] being today
 * 
 * @param {dates} paramDates an object collection of strings, keys d1...d5 which, if tests are passed are 
 * converted to full ISO dates and returned in an array
 */
const paramDatesToFull = (paramDates = {}) => {
  let dates=[today()];  // first entry in return array is today
  // console.log('got', paramDates)
  for (let i=1; i<=5; i++) {
    let date=paramDates['d'+i];

    if (typeof date !== 'string') continue;
    if (date.length !== 4) continue;
    let DD=Number(date.slice(0,2));
    let MM=Number(date.slice(2));
    if (!DD || !MM) continue;

    let YYYY=dayjs().format('YYYY');

    if (dayjs(`${YYYY}-${MM}-${DD}`).isAfter(dayjs())) YYYY--;

    dates.push(dayjs(`${YYYY}-${MM}-${DD}`).format('MM/DD/YYYY'));
  };
  return dates;
}

const today = () => dayjs().format('MM/DD/YYYY');
const datePostfix = (d, underscore=true) => (underscore ? '_' : '') + dayjs(d).format('DDMMYYYY');

Date.prototype.stdTimezoneOffset = function () {
  var jan = new Date(this.getFullYear(), 0, 1);
  var jul = new Date(this.getFullYear(), 6, 1);
  return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.isDstObserved = function () {
  return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

export {paramDatesToFull, today, datePostfix}